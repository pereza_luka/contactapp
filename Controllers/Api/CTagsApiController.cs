﻿using AutoMapper;
using ContactsApp.Models;
using ContactsApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.Controllers.Api
{

    [Route("/api/contacts/{contactId:int}/tags")]
    public class CTagsApiController : Controller
    {
        private IContactRepository _repository;
        private ILogger<CTagsApiController> _logger;

        public CTagsApiController(IContactRepository repository, ILogger<CTagsApiController> logger)
        {
            _repository = repository;
            _logger = logger;
        }
        [HttpGet("")]
        public IActionResult Get(int? contactId)
        {
            try
            {
                var contact = _repository.GetContactById(contactId);

                return Ok(Mapper.Map<IEnumerable<CTagViewModel>>(contact.Tags.ToList()));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get tags: {ex}");
            }
            return BadRequest("Failed to get tags");
        }

        [HttpPost("")]
        public async Task<IActionResult> Post(int? contactId, [FromBody]CTagViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newTag = Mapper.Map<CTag>(vm);

                    _repository.AddTag(contactId, newTag);
                    if (await _repository.SaveChangesAsync())
                    {
                        return Created($"/api/contacts/{contactId}/tags/{newTag.Tag}", Mapper.Map<CTagViewModel>(newTag));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to save new Tag:{ex}");
            }
            return BadRequest("Failed to save new Tag");
        }
        [HttpPost("delete")]
        public async Task<IActionResult> Post([FromBody]CTagListModel tagList)
        {
            if (ModelState.IsValid)
            {
                foreach (CTagViewModel ctag in tagList.Tags) {
                    var tag = Mapper.Map<CTag>(_repository.GetTagById(ctag.ID));
                    _repository.DeletePhoneNumber(tag);
                }

                if (await _repository.SaveChangesAsync())
                {
                    return Ok(Mapper.Map<IEnumerable<CTagViewModel>>(tagList.Tags.ToList()));
                }



            }
            return BadRequest("Failed to delete tags");
        }
    }

}

﻿using AutoMapper;
using ContactsApp.Models;
using ContactsApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.Controllers.Api
{
    [Route("/api/contacts")]
    public class ContactsApiController : Controller
    {
        private IContactRepository _repository;
        private ILogger<ContactsApiController> _logger;

        public ContactsApiController(IContactRepository repository, ILogger<ContactsApiController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("")]
        public IActionResult Get()
        {
            try
            {
                var results = _repository.GetAllContacts();
                return Ok(Mapper.Map<IEnumerable<ContactViewModel>>(results));
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed to get all contacts: {0}", ex);
                return BadRequest("Error occurred");
            }
        }
        [HttpGet("{contactId:int}")]

        public IActionResult Get(int? contactId)
        {
            try
            {
                var results = _repository.GetContactById(contactId);
                return Ok(Mapper.Map<Contact, ContactViewModel>(results));
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed to get all contacts: {0}", ex);
                return BadRequest($"Error occurred:{ex}");
            }
        }
        [HttpGet("uniqueTags")]

        public IActionResult GetTags()
        {
            try
            {
                var results = _repository.GetUniqueTags();
                return Ok(Mapper.Map<IEnumerable<CTagViewModel>>(results));
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed to get all contacts: {0}", ex);
                return BadRequest($"Error occurred:{ex}");
            }
        }
        [HttpGet("search/{searchParam}")]

        public IActionResult Get(string searchParam)
        {
            try
            {
                var results = _repository.GetContactBySearchParam(searchParam);
                return Ok(Mapper.Map<IEnumerable<ContactViewModel>>(results));
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed to get contacts: {0}", ex);
                return BadRequest($"Error occurred:{ex}");
            }
        }
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody]ContactViewModel theContact)
        {
            if (ModelState.IsValid)
            {
                var newContact = Mapper.Map<Contact>(theContact);
                _repository.AddContact(newContact);
                if (await _repository.SaveChangesAsync())
                {
                    return Created($"api/contacts/{theContact.FirstName}", Mapper.Map<ContactViewModel>(newContact));
                }





            }

            return BadRequest("Failed to save contact");

        }
        [HttpPut("{contactId:int}/edit")]
        public async Task<IActionResult> Put([FromBody]ContactViewModel theContact)
        {
            if (ModelState.IsValid)
            {
                var contact = Mapper.Map<Contact>(theContact);
                _repository.UpdateContact(contact);

                if (await _repository.SaveChangesAsync())
                {
                    return Created($"api/contacts/{theContact.FirstName}/edit", Mapper.Map<ContactViewModel>(contact));
                }



            }
            return BadRequest("Failed to edit contact");
        }

        [HttpDelete("{contactId:int}/delete")]
        public async Task<IActionResult> Delete(int? contactId)
        {
            if (ModelState.IsValid)
            {
                var contact = Mapper.Map<Contact>(_repository.GetContactById(contactId));
                _repository.DeleteContact(contact);
                if (await _repository.SaveChangesAsync())
                {
                    return Ok(Mapper.Map<ContactViewModel>(contact));
                }



            }
            return BadRequest("Failed to edit contact");
        }

    }
}

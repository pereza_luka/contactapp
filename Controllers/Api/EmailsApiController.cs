﻿using AutoMapper;
using ContactsApp.Models;
using ContactsApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.Controllers.Api
{
    [Route("/api/contacts/{contactId:int}/emails")]
    public class EmailsApiController : Controller
    {
        private IContactRepository _repository;
        private ILogger<EmailsApiController> _logger;

        public EmailsApiController(IContactRepository repository, ILogger<EmailsApiController> logger)
        {
            _repository = repository;
            _logger = logger;
        }
        [HttpGet("")]
        public IActionResult Get(int? contactId)
        {
            try
            {
                var contact = _repository.GetContactById(contactId);

                return Ok(Mapper.Map<IEnumerable<EmailViewModel>>(contact.Emails.ToList()));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get emails: {ex}");
            }
            return BadRequest("Failed to get emails");
        }

        [HttpPost("")]
        public async Task<IActionResult> Post(int? contactId, [FromBody]EmailViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newEmail = Mapper.Map<Email>(vm);

                    _repository.AddEmail(contactId, newEmail);
                    if (await _repository.SaveChangesAsync())
                    {
                        return Created($"/api/contacts/{contactId}/emails/{newEmail.Mail}", Mapper.Map<EmailViewModel>(newEmail));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to save new Email:{ex}");
            }
            return BadRequest("Failed to save new Email");
        }
        [HttpPost("delete")]
        public async Task<IActionResult> Post([FromBody]EmailListModel emailList)
        {
            if (ModelState.IsValid)
            {
                foreach (EmailViewModel mail in emailList.Emails)
                {
                    var email = Mapper.Map<Email>(_repository.GetEmailById(mail.ID));
                    _repository.DeleteEmail(email);
                }

                if (await _repository.SaveChangesAsync())
                {
                    return Ok(Mapper.Map<IEnumerable<EmailViewModel>>(emailList.Emails.ToList()));
                }



            }
            return BadRequest("Failed to edit contact");
        }
    }
};

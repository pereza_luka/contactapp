﻿using AutoMapper;
using ContactsApp.Models;
using ContactsApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.Controllers.Api
{


    [Route("/api/contacts/{contactId:int}/phonenumbers")]
    public class PhoneNumbersApiController : Controller
    {
        private IContactRepository _repository;
        private ILogger<PhoneNumbersApiController> _logger;

        public PhoneNumbersApiController(IContactRepository repository, ILogger<PhoneNumbersApiController> logger)
        {
            _repository = repository;
            _logger = logger;
        }
        [HttpGet("")]
        public IActionResult Get(int? contactId)
        {
            try
            {
                var contact = _repository.GetContactById(contactId);

                return Ok(Mapper.Map<IEnumerable<PhoneNumbersViewModel>>(contact.PhoneNumbers.ToList()));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get phone numbers: {ex}");
            }
            return BadRequest("Failed to get phone numbers");
        }

        [HttpPost("")]
        public async Task<IActionResult> Post(int? contactId, [FromBody]PhoneNumbersViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newPhoneNumber = Mapper.Map<PhoneNumber>(vm);

                    _repository.AddPhoneNumber(contactId, newPhoneNumber);
                    if (await _repository.SaveChangesAsync())
                    {
                        return Created($"/api/contacts/{contactId}/phonenumbers/{newPhoneNumber.PhoneNum}", Mapper.Map<PhoneNumbersViewModel>(newPhoneNumber));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to save new phone number:{ex}");
            }
            return BadRequest("Failed to save new phone number");
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Post([FromBody]PhoneNumberListModel phoneNumberList)
        {
            if (ModelState.IsValid)
            {
                foreach (PhoneNumbersViewModel phoneNum in phoneNumberList.PhoneNumbers)
                {
                    var phoneNumber = Mapper.Map<PhoneNumber>(_repository.GetPhoneNumberById(phoneNum.ID));
                    _repository.DeletePhoneNumber(phoneNumber);
                }

                if (await _repository.SaveChangesAsync())
                {
                    return Ok(Mapper.Map<IEnumerable<PhoneNumbersViewModel>>(phoneNumberList.PhoneNumbers.ToList()));
                }



            }
            return BadRequest("Failed to delete phone numbers");
        }
    }
}

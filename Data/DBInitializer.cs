﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactsApp.Models;

namespace ContactsApp.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ContactContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (!context.Contacts.Any())
            {

                var contacts = new Contact[]
                {
                    new Contact{FirstName="John",LastName="Doe",Date=DateTime.Now},
                    new Contact{FirstName="Test",LastName="Test",Date=DateTime.Now},
                };
                foreach (Contact c in contacts)
                {
                    context.Contacts.Add(c);
                };

                context.SaveChanges();
            }

            if (!context.Emails.Any())
            {

                var emails = new Email[]
                {
                    new Email{ContactID=1,Mail="john.doe@test.test"},
                    new Email{ContactID=1,Mail="john.doe@test2.test2"},
                    new Email{ContactID=2,Mail="test.test@test.test"},
                    new Email{ContactID=2,Mail="test.test@test2.test2"},

                };
                foreach (Email e in emails)
                {
                    context.Emails.Add(e);
                };

                context.SaveChanges();
            }
            if (!context.PhoneNumbers.Any())
            {
                var phoneNumbers = new PhoneNumber[]
                {
                    new PhoneNumber{ContactID=1,PhoneNum="3423423432"},
                    new PhoneNumber{ContactID=1,PhoneNum="7435423122"},
                    new PhoneNumber{ContactID=2,PhoneNum="8765427646"},
                    new PhoneNumber{ContactID=2,PhoneNum="1236766496"},
                };
                foreach (PhoneNumber pn in phoneNumbers)
                {
                    context.PhoneNumbers.Add(pn);
                };
                context.SaveChanges();
            }
            if (!context.Tags.Any())
            {
                var tags = new CTag[]
                {
                    new CTag{ContactID=1,Tag="Friend"},
                    new CTag{ContactID=1,Tag="CloseFriend"},
                    new CTag{ContactID=2,Tag="Family"},
                    new CTag{ContactID=2,Tag="Sister"},
                };
                foreach (CTag t in tags)
                {
                    context.Tags.Add(t);
                };
                context.SaveChanges();
            }
        }

    }
}

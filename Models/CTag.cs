﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.Models
{
    public class CTag
    {
        public int ID { get; set; }
        [Required]
        public string Tag { get; set; }
        public int ContactID { get; set; }

        [ForeignKey("ContactID")]
        public virtual Contact Contact { get; set; }
    }
}

﻿using ContactsApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.Models
{
    public class ContactRepository : IContactRepository
    {
        public ContactContext _context;
        private ILogger<ContactRepository> _logger;

        public ContactRepository(ContactContext context, ILogger<ContactRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void AddContact(Contact contact)
        {
            _context.Add(contact);
        
        }

        public void AddTag(int? contactId, CTag newTag)
        {
            var contact = GetContactById(contactId);

            if (contact != null)
            {
                contact.Tags.Add(newTag);
                _context.Tags.Add(newTag);
            }
        }

        public void AddEmail(int? contactId, Email newEmail)
        {
            var contact = GetContactById(contactId);

            if (contact != null)
            {
                contact.Emails.Add(newEmail);
                _context.Emails.Add(newEmail);
            }
        }

        public void AddPhoneNumber(int? contactId, PhoneNumber newPhoneNumber)
        {
            var contact = GetContactById(contactId);

            if (contact != null)
            {
                contact.PhoneNumbers.Add(newPhoneNumber);
                _context.PhoneNumbers.Add(newPhoneNumber);
            }
        }

        public void DeleteContact(Contact contact)
        {
            _context.Remove(contact);

        }

        public void DeletePhoneNumber(PhoneNumber phoneNumber)
        {
            _context.Remove(phoneNumber);
        }

        public void DeleteEmail(Email email)
        {
            _context.Remove(email);
        }

        public void DeletePhoneNumber(CTag tag)
        {
            _context.Remove(tag);
        }

        public void UpdateContact(Contact contact)
        {
            _context.Update(contact);

        }


       

        public IEnumerable<Contact> GetAllContacts()
        {
            return _context.Contacts.ToList();
        }

        public IEnumerable<CTag> GetUniqueTags()
        {
            return _context.Tags.GroupBy(t => t.Tag).Select(t => t.FirstOrDefault()).ToList();
        }

        public virtual Contact GetContactById(int? contactId)
        {
            if (contactId == null)
            {
                //add logger
                return null;
            }
                var contact = _context.Contacts
                .Include(c => c.Emails)
                .Include(c => c.PhoneNumbers)
                .Include(c => c.Tags)
                .Where(c => c.ID == contactId)
                .FirstOrDefault();

            return contact;
        }
        public virtual PhoneNumber GetPhoneNumberById(int? phoneNumberId)
        {
            if (phoneNumberId == null)
            {
                //add logger
                return null;
            }
            var phoneNumber = _context.PhoneNumbers
            .Where(ph => ph.ID == phoneNumberId)
            .FirstOrDefault();

            return phoneNumber;
        }
        public virtual CTag GetTagById(int? tagId)
        {
            if (tagId == null)
            {
                //add logger
                return null;
            }
            var tag = _context.Tags
            .Where(t => t.ID == tagId)
            .FirstOrDefault();

            return tag;
        }
        public virtual Email GetEmailById(int? emailId)
        {
            if (emailId == null)
            {
                //add logger
                return null;
            }
            var email = _context.Emails
            .Where(e => e.ID == emailId)
            .FirstOrDefault();

            return email;
        }
        public IEnumerable<Contact> GetContactBySearchParam(string searchParam)
        {
            if (searchParam == null)
            {
                //add logger
                return null;
            }
            var contacts = _context.Contacts
            .Where(c => String.Equals(c.FirstName,searchParam, StringComparison.CurrentCultureIgnoreCase) || String.Equals(c.LastName,searchParam, StringComparison.CurrentCultureIgnoreCase) ||  c.Tags.Any(t => String.Equals(t.Tag,searchParam,StringComparison.CurrentCultureIgnoreCase) && c.ID == t.ContactID))
            .ToList();

            return contacts;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }

       
    }
}

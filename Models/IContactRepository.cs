﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ContactsApp.ViewModels;

namespace ContactsApp.Models
{
    public interface IContactRepository
    {
        IEnumerable<Contact> GetAllContacts();

        Contact GetContactById(int? contactId);
        Email GetEmailById(int? emailId);
        PhoneNumber GetPhoneNumberById(int? phoneNumberId);
        CTag GetTagById(int? tagId);
        IEnumerable<CTag> GetUniqueTags();

        IEnumerable<Contact> GetContactBySearchParam(string searchParam);

        void AddContact(Contact contact);
        void AddEmail(int? contactId, Email newEmail);
        void AddPhoneNumber(int? contactId, PhoneNumber newPhoneNumber);
        void AddTag(int? contactId, CTag newTag);

        void DeleteContact(Contact contact);
        void DeletePhoneNumber(PhoneNumber phoneNumber);
        void DeleteEmail(Email email);
        void DeletePhoneNumber(CTag tag);

        void UpdateContact(Contact contact);

        

        Task<bool> SaveChangesAsync();
        
    }
}
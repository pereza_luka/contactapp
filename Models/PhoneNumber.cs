﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ContactsApp.Models
{
    public class PhoneNumber
    {
        public int ID { get; set; }
        [Required]
        public string PhoneNum { get; set; }
        public int ContactID { get; set; }

        [ForeignKey("ContactID")]
        public virtual Contact Contact { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ContactsApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ContactsApp.Models;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using ContactsApp.ViewModels;

namespace ContactsApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(config =>
                {
                    config.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                });

            services.AddDbContext<ContactContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IContactRepository, ContactRepository>();

            services.AddLogging();
        }
  


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,ILoggerFactory factory)
        {
            Mapper.Initialize(config =>
           {
               config.CreateMap<ContactViewModel, Contact>().ReverseMap();
               config.CreateMap<EmailViewModel, Email>().ReverseMap();
               config.CreateMap<PhoneNumbersViewModel, PhoneNumber>().ReverseMap();
               config.CreateMap<CTagViewModel, CTag>().ReverseMap();
           });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                factory.AddDebug(LogLevel.Information);
            }
            else
            {
                app.UseExceptionHandler("/Contact/Error");
                factory.AddDebug(LogLevel.Error);
            }

            
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Contacts}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
               name: "spa-fallback",
               defaults: new { controller = "Contacts", action = "Index" }
           );
            });
        }
    }
}

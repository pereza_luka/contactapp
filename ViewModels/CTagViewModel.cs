﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.ViewModels
{
    public class CTagViewModel
    {
        public int ID { get; set; }
        [Required]
        public string Tag { get; set; }
    }

    public class CTagListModel
    {
        public CTagViewModel[] Tags;
    }
}

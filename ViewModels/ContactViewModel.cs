﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.ViewModels
{
    public class ContactViewModel
    {
        public int ID { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public DateTime Date { get; set; } = DateTime.UtcNow;

        public EmailViewModel[] Emails;
        public PhoneNumbersViewModel[] PhoneNumbers;
        public CTagViewModel[] Tags;
    }
}

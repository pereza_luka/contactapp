﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.ViewModels
{
    public class EmailViewModel
    {
        public int ID { get; set; }
        [Required]
        [EmailAddress]
        public string Mail { get; set; }
    }

    public class EmailListModel
    {
        public EmailViewModel[] Emails;
    }
}

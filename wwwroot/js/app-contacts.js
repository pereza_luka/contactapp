﻿//app-contacts.js
(function () {

    "use strict";

    angular.module("app-contacts", ["simpleControls", "ngRoute"])
        .config(function ($routeProvider, $locationProvider) {

            $locationProvider.hashPrefix('');

            $routeProvider.when("/", {
                controller: "contactsController",
                controllerAs: "vm",
                templateUrl: "/views/contactsView.html"
            });
            $routeProvider.when("/contact/:contactId", {
                controller: "contactsController",
                controllerAs: "vm",
                templateUrl: "/views/contactView.html"
            });
            $routeProvider.when("/add", {
                controller: "contactsController",
                controllerAs: "vm",
                templateUrl: "/views/contactAddView.html"
            });

            $routeProvider.when("/edit/:editId", {
                controller: "contactsController",
                controllerAs: "vm",
                templateUrl: "/views/contactEditView.html"
            });

            $routeProvider.when("/search/:searchParam", {
                controller: "contactsController",
                controllerAs: "vm",
                templateUrl: "/views/contactsView.html"
            });
            $routeProvider.when("/searchByTag/:tagFilter", {
                controller: "contactsController",
                controllerAs: "vm",
                templateUrl: "/views/contactsView.html"
            });

            $routeProvider.otherwise({ redirectTo: "/" });

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        });


})();
﻿//contactsController.js
(function () {

    "use strict";

    angular.module("app-contacts")
        .controller("contactsController", contactsController);

    function contactsController($http, $routeParams, $timeout, $filter) {
        var vm = this;

        vm.$http = $http;
        vm.$timeout = $timeout
        vm.contactId = $routeParams.contactId;
        vm.editId = $routeParams.editId;
        vm.searchParam = $routeParams.searchParam;
        vm.searchTag = $routeParams.tagFilter;
       
       

        vm.contact = {};
     
        vm.tag = {};
        vm.email = {};
        vm.phoneNumber = {};
        vm.deleteTagsList = { tags:[]};
        vm.deleteEmailsList = {emails:[]};
        vm.deletePhoneNumbersList = {phoneNumbers:[]};
        vm.errorMessage = "";
        vm.isBusy = true;
        vm.saved = false;
        vm.isContactList = true;
        vm.isSearchList = false;
        vm.isTagList = false;

        if (vm.editId) {


            vm.contactGetOne(vm.editId);




        } else if (vm.contactId) {

            vm.contactGetOne(vm.contactId);



        } else if (vm.searchParam) {

            vm.tagsGetAllUnique();
            vm.searchContacts(vm.searchParam);
            


        } else if (vm.searchTag) {
            vm.tagsGetAllUnique();
            vm.searchContacts(vm.searchTag);


        } else {

            vm.contactGetAll()
            vm.tagsGetAllUnique();

        }
        vm.addFormEmail = function () {
            vm.contact.emails.push({ mail: '' });
        };
        vm.addFormPhone = function () {
            vm.contact.phoneNumbers.push({ phoneNum: '' });
        };
        vm.addFormTag = function () {
            vm.contact.tags.push({ tag: '' });
        };
        vm.prepareTag = function (tag, index) {
            console.log(tag)
            if (tag.id) {
                vm.deleteTagsList.tags.push(tag)
            }
            vm.contact.tags.splice(index, 1);
        }
        vm.prepareEmail = function (mail, index) {
            console.log(mail)
            if (mail.id) {
                vm.deleteEmailsList.emails.push(mail)
            }
            vm.contact.emails.splice(index, 1);
        }
        vm.preparePhoneNumber = function (phoneNumber, index) {
            console.log(phoneNumber)
            if (phoneNumber.id) {
                vm.deletePhoneNumbersList.phoneNumbers.push(phoneNumber)
            }
            vm.contact.phoneNumbers.splice(index, 1);
        }

        //contactsController.prototype
        vm.deleteContact = function (contactId, firstName, lastName) {
            /*var vm = this;*/

            console.log(vm)
            if (confirm("Are you sure you want to delete " + firstName + " " + lastName)) {
                console.log(contactId, vm)
                vm.isBusy = true;
                vm.errorMessage = "";
                vm.$http.delete("/api/contacts/" + contactId + "/delete", contactId)
                    .then(function (response) {
                        console.log(response.data);
                        vm.contacts = vm.contacts.filter(function (value) { return value.id !== contactId })
                    }, function (error) {
                        vm.errorMessage = "Failed to save new contact:" + error;
                    }).finally(function () {
                        vm.isBusy = false;
                    });

            };
        };
        
        vm.deleteTags = function () {
            console.log(vm.deleteTagsList)
            vm.$http.post("/api/contacts/" + vm.editId + "/tags/delete", vm.deleteTagsList)
                .then(function (response) {
                    console.log(response.data);

                    vm.deleteTagsList.tags = []

                }, function (error) {
                    vm.errorMessage = "Failed to delete tags:" + error;
                }).finally(function () {
                    vm.isBusy = false;

                });
        };
        vm.deleteEmails = function () {
            console.log(vm.deleteEmailsList)
            vm.$http.post("/api/contacts/" + vm.editId + "/emails/delete", vm.deleteEmailsList)
                .then(function (response) {
                    console.log(response.data);

                    vm.deleteEmailsList.emails = []

                }, function (error) {
                    vm.errorMessage = "Failed to delete emails:" + error;
                }).finally(function () {
                    vm.isBusy = false;

                });
        };
        vm.deletePhoneNumbers = function () {
            console.log(vm.deletePhoneNumbersList)
            vm.$http.post("/api/contacts/" + vm.editId + "/phonenumbers/delete", vm.deletePhoneNumbersList)
                .then(function (response) {
                    console.log(response.data);

                    vm.deletePhoneNumbersList.phoneNumbers = []

                }, function (error) {
                    vm.errorMessage = "Failed to delete phone numbers:" + error;
                }).finally(function () {
                    vm.isBusy = false;

                });
        };
    };

    contactsController.prototype.contactGetAll = function () {
        var vm = this;

        vm.contacts = [];

        vm.contact = { emails: [], phoneNumbers: [] , tags:[] };

        vm.$http.get("/api/contacts")
            .then(function (response) {
                angular.copy(response.data, vm.contacts);
            }, function (error) {
                vm.errorMessage = "Failed to load data:" + error;
            }).finally(function () {
                vm.isBusy = false;
            });
    };
    contactsController.prototype.tagsGetAllUnique = function () {
        var vm = this;

        vm.uniqueTags = [];

        vm.$http.get("/api/contacts/uniqueTags")
            .then(function (response) {
                angular.copy(response.data, vm.uniqueTags);
          
            }, function (error) {
                vm.errorMessage = "Failed to load data:" + error;
            }).finally(function () {
                vm.isBusy = false;
            });
    };
    contactsController.prototype.contactGetOne = function (id) {
        var vm = this;
        vm.$http.get("/api/contacts/" + id)
            .then(function (response) {
                angular.copy(response.data, vm.contact);
            }, function (error) {
                vm.errorMessage = "Failed to load data:" + error;
            }).finally(function () {
                vm.isBusy = false;
            });
    };
    contactsController.prototype.editContact = function () {

        var vm = this;

        vm.saved = false;
        vm.isBusy = true;
        vm.errorMessage = "";
        vm.$http.put("/api/contacts/" + vm.editId + "/edit", vm.contact)
            .then(function (response) {
                console.log(response.data);

                angular.copy(response.data, vm.contact);
                //vm.newContact = {};

            }, function (error) {
                vm.errorMessage = "Failed to save new contact:" + error;
            }).finally(function () {
                vm.isBusy = false;
                vm.saved = true;
                vm.$timeout(function () {
                    console.log(vm.saved)
                    vm.saved = false;

                }, 2000)
            });
        

        if (vm.deleteTagsList.tags.length > 0) {
            console.log("Deleting tags")
            vm.deleteTags(vm.deleteTagsList)
        }
        if (vm.deleteEmailsList.emails.length > 0) {
            console.log("Deleting emails")
            vm.deleteEmails(vm.deleteEmailsList)
        }
        if (vm.deletePhoneNumbersList.phoneNumbers.length > 0) {
            console.log("Deleting phoneNumbers")
            vm.deletePhoneNumbers(vm.deletePhoneNumbersList)
        }
   
    };
    contactsController.prototype.addContact = function () {
        var vm = this;

        vm.isBusy = true;
        vm.errorMessage = "";
        console.log(vm.contact);
        vm.$http.post("/api/contacts", vm.contact)
            .then(function (response) {
                console.log(response.data);

                vm.contacts.push(response.data);
                vm.contact = {};

            }, function (error) {
                vm.errorMessage = "Failed to save new contact:" + error;
            }).finally(function () {
                vm.isBusy = false;
            });
    };



    contactsController.prototype.addTag = function () {
        var vm = this;

        vm.isBusy = true;
        vm.errorMessage = "";
        console.log(vm.contact);
        vm.$http.post("/api/contacts/" + vm.contactId + "/tags/", vm.tag)
            .then(function (response) {
                console.log(response.data);

                vm.contact.tags.push(response.data);
                vm.tag = {};

            }, function (error) {
                vm.errorMessage = "Failed to save new contact:" + error;
            }).finally(function () {
                vm.isBusy = false;
            });
    };
    contactsController.prototype.addEmail = function () {
        var vm = this;

        vm.isBusy = true;
        vm.errorMessage = "";
        console.log(vm.contact);
        vm.$http.post("/api/contacts/" + vm.contactId + "/emails/", vm.email)
            .then(function (response) {
                console.log(response.data);

                vm.contact.emails.push(response.data);
                vm.email = {};

            }, function (error) {
                vm.errorMessage = "Failed to save new contact:" + error;
            }).finally(function () {
                vm.isBusy = false;
            });
    };
    contactsController.prototype.addPhoneNumber = function () {
        var vm = this;

        vm.isBusy = true;
        vm.errorMessage = "";
        console.log(vm.contact);
        vm.$http.post("/api/contacts/" + vm.contactId + "/phonenumbers/", vm.phoneNumber)
            .then(function (response) {
                console.log(response.data);

                vm.contact.phoneNumbers.push(response.data);
                vm.phoneNumber = {};

            }, function (error) {
                vm.errorMessage = "Failed to save new contact:" + error;
            }).finally(function () {
                vm.isBusy = false;
            });
    };
    contactsController.prototype.searchContacts = function (searchParam) {
        var vm = this;

        vm.isContactList = false;
        vm.isSearchList = true;
        vm.isTagList = true;
        vm.searchContacts = [];
        vm.isBusy = true;
        vm.errorMessage = "";
        console.log("searchParam",searchParam)
            vm.$http.get("/api/contacts/search/" + searchParam)
            .then(function (response) {
                angular.copy(response.data, vm.searchContacts);
                console.log
            }, function (error) {
                vm.errorMessage = "Failed to load data:" + error;
            }).finally(function () {
                vm.isBusy = false;
            });
    };
})();
﻿//simpleControls.js

(function () {
    "use strict";

    angular.module("simpleControls", [])
        .directive("waitCursor", waitCursor)
        .directive("searchForm", searchForm)
        .directive("searchList", searchList)
        .directive("contactList", contactList);

    function waitCursor() {
        return {
            scope: {
                show: "=displayWhen"
            },
            restrict: "E",
            templateUrl: "/views/waitCursor.html"
        };
    }

    function searchForm() {
        return {
            controller: "contactsController",
            controllerAs: "vm",
            restrict: "E",
            templateUrl: "/views/searchForm.html"
        };
    }

    function searchList() {
        return {
            scope: {
                show: "=displayWhen",
                applicantData: '=',
                function: '='
            },
            restrict: "E",
            templateUrl: "/views/contactListTable.html"
        };
    }
    function contactList() {
        return {
            scope: {
                show: "=displayWhen",
                applicantData: '=',
                function: '='
            },
            restrict: "E",
            templateUrl: "/views/contactListTable.html"
        };
    }
})();